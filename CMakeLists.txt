cmake_minimum_required(VERSION 3.10)
project(apfp_test CXX)

find_package(APFP REQUIRED)

add_executable(test test.cpp)
target_include_directories(test PRIVATE ${APFP_INCLUDE_DIRS})
target_link_libraries(test ${APFP_LIBRARIES} stdc++fs)
