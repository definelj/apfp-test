#include <apfp/config.h>
#include <apfp/matmul_library.h>
#include <mpfr.h>

#include <chrono>
#include <experimental/filesystem>
#include <iostream>
#include <random>
#include <thread>  // std::thread::hardware_concurrency
#include <vector>

class RandomNumberGenerator {
 public:
  RandomNumberGenerator() : dist_(0, 1) {
    gmp_randinit_default(state_);
  }

  ~RandomNumberGenerator() {
    gmp_randclear(state_);
  }

  __mpfr_struct GenerateNumber() {
    mpfr_t num;
    mpfr_init2(num, apfp::kBits);
    mpfr_urandom(num, state_, MPFR_RNDZ);
    return num[0];
  }

  std::vector<mpfr_t> GenerateNumbers(const int n) {
    std::vector<mpfr_t> data(n);
    for (int i = 0; i < n; ++i) {
      mpfr_t num = {GenerateNumber()};
      data[i][0] = num[0];
    }
    return data;
  }

 private:
  gmp_randstate_t state_;
  std::uniform_int_distribution<> dist_;
};

double RunMPFR(std::vector<mpfr_t> const &a, std::vector<mpfr_t> const &b, std::vector<mpfr_t> &result, const int n,
               const int k, const int m, bool a_transposed);

int main(int argc, const char **argv) {
  if (argc != 4) {
    std::cerr << "Usage: " << argv[0] << " <n> <k> <m>\n";
    return 1;
  }
  const int n = std::stoi(argv[1]);
  const int k = std::stoi(argv[2]);
  const int m = std::stoi(argv[3]);

  if (n % apfp::kTileSize != 0) {
    std::cerr << "N=" << n << " must be a multiple of the tile size " << apfp::kTileSize << ".\n";
    return 1;
  }
  if (m % apfp::kTileSize != 0) {
    std::cerr << "M=" << m << " must be a multiple of the tile size " << apfp::kTileSize << ".\n";
    return 1;
  }
  if (k <= 0) {
    std::cerr << "K must be positive.\n";
    return 1;
  }

  std::cout << "Initializing accelerator...\n" << std::flush;
  std::string kernel_path(apfp::kBitstreamDirectory + std::string("/matmul_dram_kernel_hw.xclbin"));
  if (!std::experimental::filesystem::exists(kernel_path)) {
    std::stringstream ss;
    ss << "Kernel file " << kernel_path << " not found.\n";
    throw std::runtime_error(ss.str());
  }
  apfp::Initialize(kernel_path, n, k, m);

  std::cout << "Generating input data..." << std::flush;
  RandomNumberGenerator rng;
  auto as = rng.GenerateNumbers(n * k);
  auto bs = rng.GenerateNumbers(k * m);
  auto cs = std::vector<mpfr_t>(n * m);
  auto ref = std::vector<mpfr_t>(n * m);
  for (int i = 0; i < n * m; ++i) {
    mpfr_init2(cs[i], apfp::kBits);
    mpfr_init2(ref[i], apfp::kBits);
    mpfr_set_ui(ref[i], 0, MPFR_RNDZ);
  }
  std::cout << " Done.\n";

  {
    std::cout << "Copying data to the device..." << std::flush;
    const auto begin = std::chrono::high_resolution_clock::now();
    apfp::CopyAToDevice(as.data());
    apfp::CopyBToDevice(bs.data());
    const auto elapsed =
        std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - begin).count();
    std::cout << " Done in " << 1e-9 * elapsed << " seconds.\n";
  }

  {
    std::cout << "Executing kernel..." << std::flush;
    const auto begin = std::chrono::high_resolution_clock::now();
    const auto cycles = apfp::MultiplyAxB();
    const auto elapsed =
        std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - begin).count();
    std::cout << " Done in " << 1e-9 * elapsed << " seconds.\n";
    std::cout << "Kernel reported " << cycles << " cycles.\n";
  }

  {
    std::cout << "Copying back result..." << std::flush;
    const auto begin = std::chrono::high_resolution_clock::now();
    apfp::CopyCToHost(cs.data());
    const auto elapsed =
        std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - begin).count();
    std::cout << " Done in " << 1e-9 * elapsed << " seconds.\n";
  }

  std::cout << "Running MPFR..." << std::flush;
  const auto elapsed = RunMPFR(as, bs, ref, n, k, m, true);
  std::cout << " Done in " << elapsed << " seconds.\n";

  size_t errors_found = 0;
  {
    std::cout << "Verifying results...\n" << std::flush;
    constexpr size_t kMaxErrorsToPrint = 4;
    for (int i = 0; i < n * m; ++i) {
      if (mpfr_cmp(cs[i], ref[i]) != 0) {
        if (errors_found < kMaxErrorsToPrint) {
          std::cerr << "Mismatch at index " << i << ".";
        } else if (errors_found == kMaxErrorsToPrint) {
          std::cerr << "More mismatches found...\n";
        }
        ++errors_found;
      }
    }
    if (errors_found > 0) {
      std::cerr << "Found a total of " << errors_found << " mismatches.\n";
    } else {
      std::cout << "Validation successful.\n";
    }
  }

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < k; ++j) {
      mpfr_clear(as[i * k + j]);
    }
  }
  for (int i = 0; i < k; ++i) {
    for (int j = 0; j < m; ++j) {
      mpfr_clear(bs[i * m + j]);
    }
  }
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      mpfr_clear(cs[i * m + j]);
    }
  }

  apfp::Finalize();

  return (errors_found == 0) ? 0 : 1;
}

double RunMPFR(std::vector<mpfr_t> const &a, std::vector<mpfr_t> const &b, std::vector<mpfr_t> &result,
               const int size_n, const int size_k, const int size_m, const bool a_transposed) {
  // Use maximum number of available hardware threads
  const int num_threads = std::thread::hardware_concurrency();
  const auto start = std::chrono::high_resolution_clock::now();
  mpfr_t tmp;
  mpfr_init2(tmp, apfp::kBits);
  for (int nt = 0; nt < size_n; nt += apfp::kTileSize) {
    for (int mt = 0; mt < size_m; mt += apfp::kTileSize) {
      for (int k = 0; k < size_k; ++k) {
        for (int n = nt; n < nt + apfp::kTileSize && n < size_n; ++n) {
          for (int m = mt; m < mt + apfp::kTileSize && m < size_m; ++m) {
            if (k == 0) {
              mpfr_set_ui(result[n * size_m + m], 0, MPFR_RNDZ);
            }
            const size_t index_a = (a_transposed) ? (k * size_n + n) : (n * size_k + k);
            mpfr_mul(tmp, a[index_a], b[k * size_m + m], MPFR_RNDZ);
            mpfr_add(result[n * size_m + m], result[n * size_m + m], tmp, MPFR_RNDZ);
          }
        }
      }
    }
  }
  mpfr_clear(tmp);
  const auto end = std::chrono::high_resolution_clock::now();
  return 1e-9 * std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
}
