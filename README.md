Small test project to demonstrate how the FPGA arbitrary precision matrix
multiplication kernel can be invoked.

See `CMakeLists.txt` for how to link against the installation that exposes the
accelerator.

To build this test, we need to point it to the installation of the APFP project:

```bash
mkdir build
cd build
cmake .. -DAPFP_DIR=/home/definelj/apfp/lib/cmake/APFP
make
```

The test can be run with (using, e.g., matrix sizes 128x128x128):

```bash
./test 128 128 128
```

Currently, matrix sizes must be a multiple of the tile size that the
kernel has been compiled with. This can potentially be relaxed if necessary.

Also note that the adder on the FPGA currently does not support negative
numbers. This can also be relaxed if required.
